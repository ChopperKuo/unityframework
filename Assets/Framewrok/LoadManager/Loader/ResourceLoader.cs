﻿using UnityEngine;
using System.Collections;

namespace Framework.Loader
{
	public sealed class ResourceLoader : LoadManager.Loader
	{
		private ResourceRequest _request = null;

		protected override bool Trigger()
		{
			if (_request == null)
			{
				return false;
			}

			if (_request.isDone)
			{
				asset = _request.asset;
				_request = null;
				return false;
			}
			else
			{
				return true;
			}
		}

		public ResourceLoader(string path, System.Type type) :
			base(path, type)
		{
			path = System.IO.Path.ChangeExtension(path, null);
			_request = Resources.LoadAsync(path, type);
		}

		[RuntimeInitializeOnLoadMethod]
		private static void RegisterLoader()
		{
			RegisterLoader<ResourceLoader>(LoadCategory.Resources);
		}
	}
}
