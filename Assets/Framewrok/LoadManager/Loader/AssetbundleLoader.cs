﻿using UnityEngine;
using System.Collections;

namespace Framework.Loader
{
	public class AssetbundleLoader : LoadManager.Loader
	{
		protected override bool Trigger()
		{
			return false;
		}

		public AssetbundleLoader(string path, System.Type type) :
			base(path, type)
		{
		}

		[RuntimeInitializeOnLoadMethod]
		private static void RegisterLoader()
		{
			RegisterLoader<AssetDatabaseLoader>(LoadCategory.AssetBundle);
		}
	}
}
