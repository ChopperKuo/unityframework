﻿using UnityEngine;
using System.Collections;

namespace Framework.Loader
{
	public sealed class AssetDatabaseLoader : LoadManager.Loader
	{	
		protected override bool Trigger()
		{
			return false;
		}

		public AssetDatabaseLoader(string path, System.Type type) :
			base(path, type)
		{
			asset = UnityEditor.AssetDatabase.LoadAssetAtPath(path, type);
		}

		[RuntimeInitializeOnLoadMethod]
		private static void RegisterLoader()
		{
			RegisterLoader<AssetDatabaseLoader>(LoadCategory.AssetDatabase);
		}
	}
}
