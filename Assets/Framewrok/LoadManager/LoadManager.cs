﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Framework
{
	public static class LoadManager
	{
		public abstract class Loader : AssetRequest
		{
			public Loader(string path, System.Type type) :
				base(path, type)
			{
			}

			protected static void RegisterLoader<T>(LoadCategory category)
			{
				Debug.AssertFormat(!_loadWrappers.ContainsKey(category), "The {0} loader were already registered.", category);
				_loadWrappers[category] = typeof(T);
			}
		}

		private class LoadRequest : AssetRequest
		{
			private int _loadIndex = -1;
			private Loader _currentLoader = null;

			private Loader currentLoader
			{
				get
				{
					while (_currentLoader == null)
					{
						_loadIndex++;
						LoadCategory category;
						if (!LoadConfig.Instance.TryGetLoadPriority(_loadIndex, out category))
						{
							break;
						}

						System.Type type;
						if (!_loadWrappers.TryGetValue(category, out type))
						{
							continue;
						}

						_currentLoader = System.Activator.CreateInstance(type, assetPath, assetType) as Loader;
					}
					return _currentLoader;
				}
			}

			public LoadRequest(string path, System.Type type) :
				base(path, type)
			{
			}

			protected override bool Trigger()
			{
				if (currentLoader == null)
				{
					_loadingRequests.Remove(assetPath, assetType);
					return false;
				}

				if (!_currentLoader.keepWaiting)
				{
					asset = _currentLoader.asset;
					_currentLoader = null;
				}

				if (asset != null)
				{
					_loadingRequests.Remove(assetPath, assetType);
					return false;
				}
				else
				{
					return true;
				}
			}
		}

		private class WaitRequest : AssetRequest
		{
			private AssetRequest _request = null;

			public WaitRequest(AssetRequest request) :
				base(request.assetPath, request.assetType)
			{
				_request = request;
			}

			protected override bool Trigger()
			{
				if (_request == null)
				{
					return false;
				}

				if (_request.isDone)
				{
					asset = _request.asset;
					_request = null;
					return false;
				}
				else
				{
					return true;
				}
			}
		}

		private static Dictionary<LoadCategory, System.Type> _loadWrappers = new Dictionary<LoadCategory, System.Type>();
		private static DualKeyDictionary<string, System.Type, AssetRequest> _loadingRequests = new DualKeyDictionary<string, System.Type, AssetRequest>();

		public static AssetRequest LoadAsync<T>(string path)
		{
			return CreateRequest(path, typeof(T));
		}

		public static AssetRequest LoadAsync(string path, System.Type type)
		{
			return CreateRequest(path, type);
		}

		private static AssetRequest CreateRequest(string path, System.Type type)
		{
			AssetRequest request;
			if (_loadingRequests.TryGetValue(path, type, out request))
			{
				request = new WaitRequest(request);
			}
			else
			{
				request = new LoadRequest(path, type);
				_loadingRequests.Add(path, type, request);
			}
			return request;
		}
	}
}
