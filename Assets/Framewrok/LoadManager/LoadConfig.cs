﻿using UnityEngine;
using System.Collections;

namespace Framework
{
	[CreateAssetMenu(fileName = "LoadConfig", menuName = "Config/LoadConfig")]
	public class LoadConfig : Config<LoadConfig>
	{
		public LoadCategory[] Priorities = null;

		public bool TryGetLoadPriority(int index, out LoadCategory catrgory)
		{
			if ((index >= 0) && (index < Priorities.Length))
			{
				catrgory = Priorities[index];
				return true;
			}
			else
			{
				catrgory = LoadCategory.Resources;
				return false;
			}
		}
	}

	public enum LoadCategory
	{
		AssetDatabase,
		Resources,
		AssetBundle,
	}
}
