﻿using UnityEngine;
using System.Collections;

namespace Framework
{
	/// <summary>
	/// Asynchronous load request.
	/// </summary>
	public abstract class AssetRequest : AsyncRequest
	{
		/// <summary>
		/// Asset object being loaded (Read Only).
		/// </summary>
		public Object asset { get; protected set; }

		/// <summary>
		/// Pathname of the target folder.
		/// </summary>
		public string assetPath { get; private set; }

		/// <summary>
		/// Type filter for objects loaded.
		/// </summary>
		public System.Type assetType { get; private set; }

		#region Constructors
		public AssetRequest(string path, System.Type type)
		{
			assetPath = path;
			assetType = type;
		}
		#endregion
	}
}
