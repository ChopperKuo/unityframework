﻿using UnityEngine;
using System.Collections;

namespace Framework
{
	/// <summary>
	/// Asynchronous operation coroutine.
	/// </summary>
	public abstract class AsyncRequest : CustomYieldInstruction
	{
		#region Fields
		private int _triggerFrame = 0;
		#endregion

		/// <summary>
		/// Has the operation finished? (Read Only)
		/// </summary>
		public bool isDone { get; private set; }

		#region CustomYieldInstruction
		public sealed override bool keepWaiting
		{
			get
			{
				if (!isDone)
				{
					if (_triggerFrame != Time.frameCount)
					{
						_triggerFrame = Time.frameCount;
						isDone = !Trigger();
					}
				}
				return !isDone;
			}
		}
		#endregion

		/// <summary>
		/// The Trigger is queried each frame after MonoBehaviour.Update and before MonoBehaviour.LateUpdate.
		/// </summary>
		/// <returns>Indicates if coroutine should be kept suspended. To keep coroutine suspended return true.</returns>
		protected abstract bool Trigger();
	}
}
