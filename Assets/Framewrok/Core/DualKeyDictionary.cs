﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Framework
{
	public class DualKeyDictionary<TFirstKey, TSecondKey, TValue> : Dictionary<KeyValuePair<TFirstKey, TSecondKey>, TValue>
	{
		TValue this[TFirstKey firstKey, TSecondKey secondKey]
		{
			get
			{
				return this[new KeyValuePair<TFirstKey, TSecondKey>(firstKey, secondKey)];
			}
			set
			{
				this[new KeyValuePair<TFirstKey, TSecondKey>(firstKey, secondKey)] = value;
			}
		}

		public void Add(TFirstKey firstKey, TSecondKey secondKey, TValue value)
		{
			Add(new KeyValuePair<TFirstKey, TSecondKey>(firstKey, secondKey), value);
		}

		public bool ContainsKey(TFirstKey firstKey, TSecondKey secondKey)
		{
			return ContainsKey(new KeyValuePair<TFirstKey, TSecondKey>(firstKey, secondKey));
		}

		public void Remove(TFirstKey firstKey, TSecondKey secondKey)
		{
			Remove(new KeyValuePair<TFirstKey, TSecondKey>(firstKey, secondKey));
		}

		public bool TryGetValue(TFirstKey firstKey, TSecondKey secondKey, out TValue value)
		{
			return TryGetValue(new KeyValuePair<TFirstKey, TSecondKey>(firstKey, secondKey), out value);
		}
	}
}
