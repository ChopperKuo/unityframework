﻿using UnityEngine;
using System.Collections;

namespace Framework
{
	public class Config<T> : ScriptableObject
		where T : Config<T>
	{
		private static T _instance;

		public static T Instance
		{
			get
			{
				if (_instance == null)
				{
					_instance = Resources.Load<T>(typeof(T).Name);
				}
				return _instance;
			}
		}
	}
}
